package com.flowkode.terrastate

class AlreadyLockedException(val lockHolder: LockInfo) : Exception()