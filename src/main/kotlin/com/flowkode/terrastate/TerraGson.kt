package com.flowkode.terrastate

import com.google.gson.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

class TerraGson {
    companion object {
        fun build(): Gson {
            val pattern = DateTimeFormatterBuilder()
                    .appendPattern("yyyy-MM-dd")
                    .appendLiteral("T")
                    .appendPattern("HH:mm:ss")
                    .appendFraction(ChronoField.MICRO_OF_SECOND, 0, 9, true)
                    .appendLiteral("Z")
                    .toFormatter()

            return GsonBuilder()
                    .registerTypeAdapter(LocalDateTime::class.java, JsonDeserializer<LocalDateTime> { json, _, _ ->
                        var ret: LocalDateTime? = null
                        if (json.asJsonPrimitive.isString) {
                            ret = LocalDateTime.parse(json.asJsonPrimitive.asString, pattern)
                        }
                        return@JsonDeserializer ret
                    })
                    .registerTypeAdapter(LocalDateTime::class.java, JsonSerializer<LocalDateTime> { src, _, _ ->
                        return@JsonSerializer JsonPrimitive(src.format(pattern))
                    })
                    .setFieldNamingStrategy({ f ->
                        if (f.name.equals("id", true)) {
                            "ID"
                        }
                        else {
                            f.name.capitalize()
                        }
                    })
                    .create()
        }
    }
}