package com.flowkode.terrastate

import com.google.gson.Gson
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.time.temporal.TemporalAccessor
import java.util.*
import kotlin.streams.asSequence


class StateService(baseDir: Path, private val history: Boolean, private val gson: Gson) {

    private val statePath = baseDir.normalize().toAbsolutePath()

    private val historyPath = statePath.resolve("history")

    private val lockFile = statePath.resolve("state.lock")

    private val stateFile = statePath.resolve("state")

    companion object {
        private val fileCharset = StandardCharsets.UTF_8

        private var pattern = DateTimeFormatterBuilder()
                .appendPattern("yyyyMMddHHmmss")
                .appendValue(ChronoField.MILLI_OF_SECOND, 3)
                .toFormatter()
                .withLocale(Locale.ENGLISH)
                .withZone(ZoneOffset.UTC)
    }

    init {
        Files.createDirectories(statePath)
        if (history) {
            Files.createDirectories(historyPath)
        }
    }

    private fun writeLockFile(lockInfo: LockInfo) {
        Files.write(lockFile, gson.toJson(lockInfo).toByteArray(fileCharset), StandardOpenOption.CREATE_NEW, StandardOpenOption.SYNC)
    }

    private fun loadLockFile(): LockInfo {
        return gson.fromJson(Files.readAllBytes(lockFile).toString(fileCharset), LockInfo::class.java)
    }

    private fun deleteLockFile() {
        Files.delete(lockFile)
    }

    private fun writeStateFile(state: String) {
        handleHistory()
        Files.write(stateFile, state.toByteArray(fileCharset), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.SYNC)
    }

    private fun loadStateFile(): String {
        if (Files.exists(stateFile)) {
            return Files.readAllBytes(stateFile).toString(fileCharset)
        }
        return ""
    }

    private fun deleteStateFile() {
        writeStateFile("")
    }

    private fun handleHistory() {
        if (history && Files.exists(stateFile)) {
            Files.copy(stateFile, historyPath.resolve(pattern.format(Instant.now())))
        }
    }

    private fun isLocked(): Boolean {
        return Files.exists(lockFile) && Files.isRegularFile(lockFile)
    }

    private fun canPerformAction(id: String): Boolean {
        return isLocked() && loadLockFile().id == id
    }

    fun lock(lockInfo: LockInfo) {
        if (isLocked()) {
            throw AlreadyLockedException(loadLockFile())
        }
        else {
            writeLockFile(lockInfo)
        }
    }

    fun unlock(lockInfo: LockInfo) {
        if (isLocked()) {
            if (canPerformAction(lockInfo.id)) {
                deleteLockFile()
            }
            else {
                throw AlreadyLockedException(loadLockFile())
            }
        }
    }

    fun getState(): String {
        return loadStateFile()
    }

    fun setState(id: String?, state: String) {
        if (id == null) {
            if (isLocked()) {
                throw AlreadyLockedException(loadLockFile())
            }
            else {
                val internalLock = LockInfo("System", "Internal", "Internal lock for actions without ID", "System", "0", LocalDateTime.now(), "/")
                lock(internalLock)
                writeStateFile(state)
                unlock(internalLock)
            }
        }
        else if (!isLocked()) {
            throw NotLockedException()
        }
        else if (canPerformAction(id)) {
            writeStateFile(state)
        }
        else {
            throw AlreadyLockedException(loadLockFile())
        }
    }

    fun delete(id: String) {
        if (!isLocked()) {
            throw NotLockedException()
        }
        if (canPerformAction(id)) {
            if (stateFileExists()) {
                deleteStateFile()
            }
        }
        else {
            throw AlreadyLockedException(loadLockFile())
        }
    }

    private fun stateFileExists(): Boolean {
        return Files.exists(stateFile)
    }

    fun listHistory(): List<TemporalAccessor> {
        return try {
            Files.list(historyPath)
                    .asSequence()
                    .sorted()
                    .map { p -> p.fileName.toString() }
                    .map { s -> LocalDateTime.parse(s, pattern).toInstant(ZoneOffset.UTC) }
                    .toList()
        }
        catch (ex: IOException) {
            Collections.emptyList()
        }
    }

    fun getHistory(timestamp: TemporalAccessor): String {
        val file = historyPath.resolve(pattern.format(timestamp))
        if (Files.exists(file)) {
            return Files.readAllBytes(file).toString(fileCharset)
        }
        return ""
    }

}
