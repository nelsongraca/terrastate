package com.flowkode.terrastate

import spark.Response
import spark.kotlin.before
import spark.kotlin.halt
import java.util.*


class AuthController(authService: AuthService) {
    init {
        before("/*") {
            val authorization = request.headers("Authorization")
            if (authorization == null) {
                requestAuthentication(response)
            }
            else {
                val (username, password) = String(Base64.getDecoder().decode(authorization.replace("Basic", "").trim())).split(":")
                try {
                    authService.authenticate(username, password.toCharArray())
                }
                catch (ex: InvalidCredentialsException) {
                    requestAuthentication(response)
                }
            }
        }
    }

    private fun requestAuthentication(response: Response) {
        response.header("WWW-Authenticate", "Basic realm=\"TerraState\"")
        halt(401)
    }
}
