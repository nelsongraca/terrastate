package com.flowkode.terrastate

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.nio.file.Path
import java.nio.file.Paths

class TerraStateArgs(parser: ArgParser) {
    val workDir: Path by parser.storing(
            "-d", "--data-directory",
            help = "The data directory, state and users are stored on it, defaults to the working directory.") {
        Paths.get(this)
    }.default(Paths.get("."))

    val generatePassword by parser.flagging(
            "-g", "--generate-password",
            help = "Generate an encrypted password."
    )

    val history by parser.flagging(
            "--history",
            help = "Keep History of the state file."
    ).default(false)

    val addUser by parser.storing(
            "-a", "--add-user",
            help = "Add a new user.",
            argName = "USERNAME"
    ).default("")

    val removeUser by parser.storing(
            "-r", "--remove-user",
            help = "Remove a user.",
            argName = "USERNAME"
    ).default("")
}