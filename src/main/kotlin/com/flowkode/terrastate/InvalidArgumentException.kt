package com.flowkode.terrastate

class InvalidArgumentException(message: Messages) : Exception() {
    val errorMessage = message.errorMessage

    enum class Messages(val errorMessage: String) {
        EMPTY_PASSWORD("You must provide a password."),
        EMPTY_USERNAME("You must provide a username."),
        USER_ALREADY_EXISTS("The provided user already exists."),
        USER_DOES_NOT_EXIST("The provided user does not exist.")
    }
}
