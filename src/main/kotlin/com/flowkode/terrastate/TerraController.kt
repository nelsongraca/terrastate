package com.flowkode.terrastate

import com.google.gson.Gson
import spark.kotlin.*


class TerraController(private val stateService: StateService, private val gson: Gson) {

    companion object {
        private const val OK = 200

        private const val LOCKED = 423

        private const val PRECONDITION_REQUIRED = 428
    }

    init {
        after("/") {
            if ("LOCK" == requestMethod()) {
                try {
                    stateService.lock(gson.fromJson<LockInfo>(request.body(), LockInfo::class.java))
                    halt(OK)
                }
                catch (ex: AlreadyLockedException) {
                    halt(LOCKED, gson.toJson(ex.lockHolder))
                }
            }
            else if ("UNLOCK" == requestMethod()) {
                try {
                    stateService.unlock(gson.fromJson<LockInfo>(request.body(), LockInfo::class.java))
                    halt(OK)
                }
                catch (ex: AlreadyLockedException) {
                    halt(LOCKED, gson.toJson(ex.lockHolder))
                }
            }
        }
        get("/") {
            stateService.getState()
        }
        post("/") {
            try {
                val state = request.body()
                stateService.setState(request.queryMap("ID").value(), state)
                halt(OK)
            }
            catch (ex: AlreadyLockedException) {
                halt(LOCKED, gson.toJson(ex.lockHolder))
            }
            catch (ex: NotLockedException) {
                halt(PRECONDITION_REQUIRED)
            }
        }
        delete("/") {
            try {
                stateService.delete(request.queryMap("ID").value())
            }
            catch (ex: AlreadyLockedException) {
                halt(LOCKED, gson.toJson(ex.lockHolder))
            }
            catch (ex: NotLockedException) {
                halt(PRECONDITION_REQUIRED)
            }
        }
    }
}
