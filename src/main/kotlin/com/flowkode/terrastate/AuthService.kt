package com.flowkode.terrastate

import java.io.FileInputStream
import java.io.FileOutputStream
import java.math.BigInteger
import java.nio.file.Files
import java.nio.file.Path
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec


class AuthService(baseDir: Path) {
    companion object {
        private const val PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1"

        private const val PBKDF2_SALT_BYTES = 24

        private const val PBKDF2_ITERATIONS = 1000

        private const val HASH_BYTES = 24
    }

    private val usersFile = baseDir.resolve("users.properties").normalize().toAbsolutePath()

    private val users = Properties()

    init {
        Files.createDirectories(baseDir)
        loadUsers()
    }

    private var lastUserLoadTime = 0L

    @Synchronized
    private fun loadUsers() {
        if (lastUserLoadTime < System.currentTimeMillis()) {
            ensureUsersFileExists()
            users.clear()
            users.load(FileInputStream(usersFile.toFile()))
            lastUserLoadTime = System.currentTimeMillis()
        }
    }

    private fun ensureUsersFileExists() {
        if (!Files.exists(usersFile)) {
            Files.createFile(usersFile)
        }
    }

    @Synchronized
    fun authenticate(username: String, password: CharArray) {
        loadUsers()
        if (users.containsKey(username)) {
            if (validatePassword(password, users.getProperty(username))) {
                return
            }
        }
        throw InvalidCredentialsException()
    }

    fun encryptPassword(password: CharArray): String {
        //ensure its not empty
        if (password.isEmpty()) {
            throw InvalidArgumentException(InvalidArgumentException.Messages.EMPTY_PASSWORD)
        }

        // Generate a random salt
        val random = SecureRandom()
        val salt = ByteArray(PBKDF2_SALT_BYTES)
        random.nextBytes(salt)

        // Hash the password
        val hash = pbkdf2Internal(password, salt, PBKDF2_ITERATIONS, HASH_BYTES)
        // format iterations:salt:hash
        return "$PBKDF2_ITERATIONS:${toHex(salt)}:${toHex(hash)}"
    }

    private fun validatePassword(password: CharArray, goodHash: String): Boolean {
        // Decode the hash into its parameters
        val params = goodHash.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val iterations = Integer.parseInt(params[0])
        val salt = fromHex(params[1])
        val hash = fromHex(params[2])
        // Compute the hash of the provided password, using the same salt,
        // iteration count, and hash length
        val testHash = pbkdf2Internal(password, salt, iterations, hash.size)
        // Compare the hashes in constant time. The password is correct if
        // both hashes match.
        return hash.contentEquals(testHash)
    }

    private fun pbkdf2Internal(password: CharArray, salt: ByteArray, iterations: Int, bytes: Int): ByteArray {
        val spec = PBEKeySpec(password, salt, iterations, bytes * 8)
        try {
            return SecretKeyFactory.getInstance(PBKDF2_ALGORITHM).generateSecret(spec).encoded
        }
        catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Could not hash password.", e)
        }
        catch (e: InvalidKeySpecException) {
            throw RuntimeException("Could not hash password.", e)
        }
    }

    private fun toHex(array: ByteArray): String {
        val bi = BigInteger(1, array)
        val hex = bi.toString(16)
        val paddingLength = array.size * 2 - hex.length
        return if (paddingLength > 0) {
            String.format("%0" + paddingLength + "d", 0) + hex
        }
        else {
            hex
        }
    }

    private fun fromHex(hex: String): ByteArray {
        val binary = ByteArray(hex.length / 2)
        for (i in binary.indices) {
            binary[i] = Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16).toByte()
        }
        return binary
    }

    @Synchronized
    fun addUser(username: String, password: CharArray) {
        if (username.isEmpty()) {
            throw InvalidArgumentException(InvalidArgumentException.Messages.EMPTY_USERNAME)
        }

        if (users.containsKey(username)) {
            throw InvalidArgumentException(InvalidArgumentException.Messages.USER_ALREADY_EXISTS)
        }
        val encryptedPassword = encryptPassword(password)
        users[username] = encryptedPassword
        ensureUsersFileExists()
        users.store(FileOutputStream(usersFile.toFile()), "")
    }

    @Synchronized
    fun removeUser(username: String) {
        if (!users.containsKey(username)) {
            throw InvalidArgumentException(InvalidArgumentException.Messages.USER_DOES_NOT_EXIST)
        }
        users.remove(username)
        ensureUsersFileExists()
        users.store(FileOutputStream(usersFile.toFile()), "")
    }
}
