package com.flowkode.terrastate

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import org.koin.Koin
import org.koin.dsl.module.applicationContext
import org.koin.log.EmptyLogger
import org.koin.spark.controller
import org.koin.spark.runControllers
import org.koin.spark.start
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject

fun main(args: Array<String>) {
    Koin.logger = EmptyLogger()
    Main().run(args)
}

class Main : KoinComponent {
    fun run(args: Array<String>) {
        try {
            ArgParser(args).parseInto(::TerraStateArgs).run {
                val helloAppModule = applicationContext {
                    bean { TerraGson.build() }
                    bean { StateService(workDir, history, get()) }
                    bean { AuthService(workDir) }
                    controller { AuthController(get()) }
                    controller { TerraController(get(), get()) }
                }
                when {
                    generatePassword -> {
                        startKoin(listOf(helloAppModule))
                        val authService: AuthService by inject()
                        println(authService.encryptPassword(askPassword()))
                    }
                    addUser.isNotEmpty() -> {
                        startKoin(listOf(helloAppModule))
                        val authService: AuthService by inject()
                        authService.addUser(addUser, askPassword())
                    }
                    removeUser.isNotEmpty() -> {
                        startKoin(listOf(helloAppModule))
                        val authService: AuthService by inject()
                        authService.removeUser(removeUser)
                    }
                    else -> start(modules = listOf(helloAppModule), port = 9999) {
                        runControllers()
                    }
                }
                return
            }
        }
        catch (ex: InvalidArgumentException) {
            println(ex.errorMessage)
        }
        catch (ex: SystemExitException) {
            ex.printAndExit("terraState")
        }
    }

    private fun askPassword(): CharArray {
        return System.console().readPassword("Password:")
    }
}
