package com.flowkode.terrastate

import java.time.LocalDateTime

data class LockInfo(
        val id: String,
        val operation: String,
        val info: String,
        val who: String,
        val version: String,
        val created: LocalDateTime,
        val path: String)
