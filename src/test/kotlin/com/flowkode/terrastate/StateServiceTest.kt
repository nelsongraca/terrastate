package com.flowkode.terrastate

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import java.nio.file.Files
import java.time.LocalDateTime
import kotlin.test.BeforeTest
import kotlin.test.Test

class StateServiceTest {

    private lateinit var stateService: StateService

    private val id1 = "ID1"

    private val id2 = "ID2"

    private val simpleState = "{\"cenas\":\"coisas\"}"

    private val lockInfo1 = LockInfo(id1, "test", "test", id1, "1.0", LocalDateTime.now(), "/")

    private val lockInfo2 = LockInfo(id2, "test", "test", id2, "1.0", LocalDateTime.now(), "/")

    @BeforeTest
    fun beforeTest() {
        //initialize before each test so we have a clean data file
        stateService = StateService(Files.createTempDirectory("authTest"), true, TerraGson.build())
    }

    @Test
    fun lock() {
        stateService.lock(lockInfo1)
    }

    @Test(AlreadyLockedException::class)
    fun lockTwice() {
        stateService.lock(lockInfo1)
        stateService.lock(lockInfo2)
    }

    @Test
    fun lockUnlockLock() {
        stateService.lock(lockInfo1)
        stateService.unlock(lockInfo1)
        stateService.lock(lockInfo2)
    }

    @Test
    fun unlock() {
        stateService.lock(lockInfo1)
        stateService.unlock(lockInfo1)
    }

    @Test
    fun unlockUnlocked() {
        stateService.unlock(lockInfo1)
    }

    @Test(AlreadyLockedException::class)
    fun unlockNoPerms() {
        stateService.lock(lockInfo1)
        stateService.unlock(lockInfo2)
    }

    @Test(NotLockedException::class)
    fun setStateUnlocked() {
        stateService.setState(id1, simpleState)
    }

    @Test(AlreadyLockedException::class)
    fun setStateNotOwner() {
        stateService.lock(lockInfo1)
        stateService.setState(id2, simpleState)
    }

    @Test
    fun setState() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
    }

    @Test
    fun getStateClean() {
        assertThat(stateService.getState(), equalTo(""))
    }

    @Test
    fun getState() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
        assertThat(stateService.getState(), equalTo(simpleState))
    }

    @Test(NotLockedException::class)
    fun deleteWithoutLock() {
        stateService.delete(id1)
    }

    @Test(AlreadyLockedException::class)
    fun deleteWithoutAccess() {
        stateService.lock(lockInfo1)
        stateService.delete(id2)
    }

    @Test
    fun deleteWithoutState() {
        stateService.lock(lockInfo1)
        stateService.delete(id1)
        assertThat(stateService.getState(), equalTo(""))
    }

    @Test
    fun deleteWithState() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
        assertThat(stateService.getState(), equalTo(simpleState))
        stateService.unlock(lockInfo1)

        stateService.lock(lockInfo1)
        stateService.delete(id1)
        assertThat(stateService.getState(), equalTo(""))
    }

    @Test
    fun deleteHistory() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
        stateService.delete(id1)
        assertThat(stateService.listHistory().size, equalTo(1))
    }

    @Test
    fun checkHistory() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
        stateService.delete(id1)
        assertThat(stateService.getHistory(stateService.listHistory()[0]), equalTo(simpleState))
    }

    @Test
    fun checkEmptyHistory() {
        stateService.lock(lockInfo1)
        stateService.setState(id1, simpleState)
        stateService.delete(id1)
        stateService.setState(id1, simpleState)
        assertThat(stateService.getHistory(stateService.listHistory()[1]), equalTo(""))
    }

    @Test
    fun stateMv() {
        stateService.setState(null, simpleState)
    }
}
