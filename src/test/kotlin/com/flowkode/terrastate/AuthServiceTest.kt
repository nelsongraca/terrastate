package com.flowkode.terrastate

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import java.nio.file.Files
import kotlin.test.BeforeTest
import kotlin.test.Test

class AuthServiceTest {

    private lateinit var authService: AuthService

    private val validPassword = "password".toCharArray()

    private val validUser = "username"

    private val empty = ""

    @BeforeTest
    fun beforeTest() {
        //initialize before each test so we have a clean data file
        authService = AuthService(Files.createTempDirectory("authTest"))
    }

    @Test(InvalidArgumentException::class)
    fun encryptPasswordEmpty() {
        authService.encryptPassword(empty.toCharArray())
    }

    @Test
    fun encryptPassword() {
        val password = authService.encryptPassword(validPassword)
        assertThat(password.split(":").size, equalTo(3))
    }

    @Test(InvalidArgumentException::class)
    fun addUserEmptyUser() {
        authService.addUser(empty, validPassword)
    }

    @Test(InvalidArgumentException::class)
    fun addUserEmptyPassword() {
        authService.addUser(validUser, empty.toCharArray())
    }

    @Test
    fun addUser() {
        authService.addUser(validUser, validPassword)
        //now do the auth
        authService.authenticate(validUser, validPassword)
    }

    @Test(InvalidArgumentException::class)
    fun addUserExists() {
        authService.addUser(validUser, validPassword)
        authService.addUser(validUser, validPassword)
    }

    @Test(InvalidCredentialsException::class)
    fun authenticateCleanEmptyUser() {
        authService.authenticate(empty, validPassword)
    }

    @Test(InvalidCredentialsException::class)
    fun authenticateCleanEmptyPassword() {
        authService.authenticate(validUser, empty.toCharArray())
    }

    @Test(InvalidCredentialsException::class)
    fun authenticateWithDataEmptyUser() {
        authService.addUser(validUser, validPassword)
        authService.authenticate(empty, validPassword)
    }

    @Test(InvalidCredentialsException::class)
    fun authenticateWithDataEmptyPassword() {
        authService.addUser(validUser, validPassword)
        authService.authenticate(validUser, empty.toCharArray())
    }

    @Test
    fun authenticateWithData() {
        authService.addUser(validUser, validPassword)
        authService.authenticate(validUser, validPassword)
    }

    @Test(InvalidArgumentException::class)
    fun removeUserDoesNotExist() {
        authService.removeUser(validUser)
    }

    @Test()
    fun removeUser() {
        //add
        authService.addUser(validUser, validPassword)
        //remove
        authService.removeUser(validUser)
        //add again, so we can ensure it was really removed
        authService.addUser(validUser, validPassword)
    }
}