package com.flowkode.terrastate

import com.despegar.http.client.*
import com.despegar.sparkjava.test.SparkServer
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.ClassRule
import spark.servlet.SparkApplication
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.net.HttpURLConnection
import java.nio.file.Files
import java.time.LocalDateTime
import java.util.*
import kotlin.test.Test


class TerraControllerTest {


    class TestControllerTestSparkApplication : SparkApplication {
        override fun init() {
            val gson = TerraGson.build()
            TerraController(StateService(Files.createTempDirectory("authTest"), true, gson), gson)
        }
    }

    companion object {
        @ClassRule
        @JvmField
        val server: SparkServer<TestControllerTestSparkApplication> = SparkServer(TerraControllerTest.TestControllerTestSparkApplication::class.java, 5678)

        init {
            allowMethods("LOCK")
            allowMethods("UNLOCK")
        }

        private fun allowMethods(vararg methods: String) {
            try {
                val methodsField = HttpURLConnection::class.java.getDeclaredField("methods")

                val modifiersField = Field::class.java.getDeclaredField("modifiers")
                modifiersField.isAccessible = true
                modifiersField.setInt(methodsField, methodsField.modifiers and Modifier.FINAL.inv())

                methodsField.isAccessible = true

                val oldMethods = methodsField.get(null) as Array<String>
                val methodsSet = LinkedHashSet(Arrays.asList(*oldMethods))
                methodsSet.addAll(Arrays.asList(*methods))
                val newMethods = methodsSet.toTypedArray()

                methodsField.set(null, newMethods)
            }
            catch (e: NoSuchFieldException) {
                throw IllegalStateException(e)
            }
            catch (e: IllegalAccessException) {
                throw IllegalStateException(e)
            }
        }

        private const val simpleState = "{\"cenas\":\"coisas\"}"

        private const val id1 = "ID1"

        private const val id2 = "ID2"

        private val lockInfo1 = TerraGson.build().toJson(LockInfo(id1, "test", "test", id1, "1.0", LocalDateTime.now(), "/"))

        private val lockInfo2 = TerraGson.build().toJson(LockInfo(id2, "test", "test", id2, "1.0", LocalDateTime.now(), "/"))

        private const val OK = 200

        private const val LOCKED = 423

        private const val PRECONDITION_REQUIRED = 428
    }

    @Test
    fun lock() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo1)
        }
    }


    @Test
    fun lockTwice() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(lock(lockInfo2).code(), equalTo(LOCKED))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo1)
        }
    }

    @Test
    fun lockUnlockLock() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(unlock(lockInfo1).code(), equalTo(OK))
            assertThat(lock(lockInfo2).code(), equalTo(OK))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo2)
        }
    }

    @Test
    fun unlock() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(unlock(lockInfo1).code(), equalTo(OK))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo1)
        }
    }

    @Test
    fun unlockUnlocked() {
        assertThat(unlock(lockInfo1).code(), equalTo(OK))
    }

    @Test
    fun unlockNoPerms() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(unlock(lockInfo2).code(), equalTo(LOCKED))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo1)
        }
    }

    @Test
    fun setStateUnlocked() {
        assertThat(setState(lockInfo1, simpleState).code(), equalTo(PRECONDITION_REQUIRED))
    }

    @Test
    fun setStateNotOwner() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(setState(id2, simpleState).code(), equalTo(LOCKED))
        } finally {
            //unlock to keep it clean
            unlock(lockInfo1)
        }
    }

    @Test
    fun setState() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(setState(id1, simpleState).code(), equalTo(OK))
        } finally {
            //unlock to keep it clean
            delete(id1)
            unlock(lockInfo1)
        }
    }

    @Test
    fun setStateWithoutLock() {
        assertThat(setState(simpleState).code(), equalTo(OK))
    }

    @Test
    fun getStateClean() {
        val state = getState()
        assertThat(state.code(), equalTo(OK))
        assertThat(String(state.body()), equalTo(""))
    }

    @Test
    fun getStateT() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(setState(id1, simpleState).code(), equalTo(OK))
            val state = getState()
            assertThat(state.code(), equalTo(OK))
            assertThat(simpleState, equalTo(String(state.body())))
        } finally {
            delete(id1)
            unlock(lockInfo1)
        }
    }

    @Test
    fun deleteWithoutLock() {
        assertThat(delete(id1).code(), equalTo(PRECONDITION_REQUIRED))
    }

    @Test
    fun deleteWithoutAccess() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(delete(id2).code(), equalTo(LOCKED))
        } finally {
            unlock(lockInfo1)
        }
    }

    @Test
    fun deleteWithoutState() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(delete(id1).code(), equalTo(OK))
            val state = getState()
            assertThat(state.code(), equalTo(OK))
            assertThat(String(state.body()), equalTo(""))
        } finally {
            unlock(lockInfo1)
        }
    }

    @Test
    fun deleteWithState() {
        try {
            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(setState(id1, simpleState).code(), equalTo(OK))
            val state1 = getState()
            assertThat(state1.code(), equalTo(OK))
            assertThat(String(state1.body()), equalTo(simpleState))

            assertThat(unlock(lockInfo1).code(), equalTo(OK))

            assertThat(lock(lockInfo1).code(), equalTo(OK))
            assertThat(delete(id1).code(), equalTo(OK))

            val state2 = getState()
            assertThat(state2.code(), equalTo(OK))
            assertThat(String(state2.body()), equalTo(""))
        } finally {
            unlock(lockInfo1)
        }
    }

    private fun lock(body: String): HttpResponse {
        val request = LockMethod("http://localhost:" + 5678 + "/", body, false)
        return server.execute(request)
    }

    private fun unlock(body: String): HttpResponse {
        val request = UnlockMethod("http://localhost:" + 5678 + "/", body, false)
        return server.execute(request)
    }

    private fun setState(id: String, state: String): HttpResponse {
        val request = PostMethod("http://localhost:" + 5678 + "/?ID=" + id, state, false)
        return server.execute(request)
    }

    private fun setState(state: String): HttpResponse {
        val request = PostMethod("http://localhost:" + 5678, state, false)
        return server.execute(request)
    }

    private fun delete(id: String): HttpResponse {
        val request = WithoutBodyMethod("http://localhost:" + 5678 + "/?ID=" + id, "DELETE", false)
        return server.execute(request)
    }

    private fun getState(): HttpResponse {
        val request = GetMethod("http://localhost:" + 5678 + "/", false)
        return server.execute(request)
    }

    class LockMethod(url: String, body: String, followRedirects: Boolean) : WithBodyMethod(url, "LOCK", body, followRedirects)

    class UnlockMethod(url: String, body: String, followRedirects: Boolean) : WithBodyMethod(url, "UNLOCK", body, followRedirects)

}

