package com.flowkode.terrastate

import com.despegar.sparkjava.test.SparkServer
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.ClassRule
import spark.servlet.SparkApplication
import java.nio.file.Files
import java.util.*
import kotlin.test.Test


class AuthControllerTest {


    class TestContollerTestSparkApplication : SparkApplication {
        override fun init() {
            val authService = AuthService(Files.createTempDirectory("authTest"))
            authService.addUser(validUsername, validPassword.toCharArray())
            AuthController(authService)
        }
    }

    companion object {
        private const val validPassword = "password"

        private const val validUsername = "user"

        private val validUser = "Basic " + Base64.getEncoder().encodeToString("$validUsername:$validPassword".toByteArray())

        private const val invalidPassword = "badPassword"

        private const val invalidUsername = "badUser"

        private val invalidUser = "Basic " + Base64.getEncoder().encodeToString("$invalidUsername:$invalidPassword".toByteArray())

        @ClassRule
        @JvmField
        val testServer: SparkServer<TestContollerTestSparkApplication> = SparkServer(AuthControllerTest.TestContollerTestSparkApplication::class.java, 4567)
    }


    @Test
    fun requestsAuthentication() {
        val get = testServer.get("/", false)
        val httpResponse = testServer.execute(get)
        assertThat(401, equalTo(httpResponse.code()))
        assertThat("Basic realm=\"TerraState\"", equalTo(httpResponse.headers()["WWW-Authenticate"]!![0]))
    }

    @Test
    fun validCredentials() {
        val get = testServer.get("/", false)
        get.addHeader("Authorization", validUser)
        val httpResponse = testServer.execute(get)
        assertThat(404, equalTo(httpResponse.code()))
    }

    @Test
    fun invalidCredentials() {
        val get = testServer.get("/", false)
        get.addHeader("Authorization", invalidUser)
        val httpResponse = testServer.execute(get)
        assertThat(401, equalTo(httpResponse.code()))
        assertThat("Basic realm=\"TerraState\"", equalTo(httpResponse.headers()["WWW-Authenticate"]!![0]))
    }
}